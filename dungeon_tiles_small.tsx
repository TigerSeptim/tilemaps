<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="dungeon_tiles" tilewidth="8" tileheight="8" tilecount="2208" columns="46">
 <image source="dungeon_tiles.png" trans="ffffff" width="368" height="384"/>
 <wangsets>
  <wangset name="moat" type="corner" tile="-1">
   <wangcolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="" color="#00ff00" tile="-1" probability="1"/>
   <wangcolor name="" color="#0000ff" tile="-1" probability="1"/>
   <wangcolor name="" color="#ff7700" tile="-1" probability="1"/>
   <wangtile tileid="267" wangid="0,0,0,4,0,0,0,0"/>
   <wangtile tileid="268" wangid="0,0,0,4,0,4,0,0"/>
   <wangtile tileid="269" wangid="0,0,0,4,0,4,0,0"/>
   <wangtile tileid="270" wangid="0,0,0,0,0,4,0,0"/>
   <wangtile tileid="313" wangid="0,4,0,4,0,0,0,0"/>
   <wangtile tileid="314" wangid="0,4,0,4,0,4,0,4"/>
   <wangtile tileid="316" wangid="0,0,0,0,0,4,0,4"/>
   <wangtile tileid="359" wangid="0,4,0,4,0,0,0,0"/>
   <wangtile tileid="362" wangid="0,0,0,0,0,4,0,4"/>
   <wangtile tileid="405" wangid="0,4,0,2,0,2,0,2"/>
   <wangtile tileid="406" wangid="0,4,0,2,0,2,0,4"/>
   <wangtile tileid="407" wangid="0,4,0,0,0,0,0,4"/>
   <wangtile tileid="408" wangid="0,0,0,0,0,0,0,4"/>
   <wangtile tileid="489" wangid="0,0,0,1,0,0,0,0"/>
   <wangtile tileid="490" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="491" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="492" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="535" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="536" wangid="0,1,0,0,0,1,0,1"/>
   <wangtile tileid="537" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="538" wangid="0,1,0,2,0,2,0,1"/>
   <wangtile tileid="540" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="581" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="582" wangid="0,0,0,0,0,1,0,1"/>
   <wangtile tileid="583" wangid="0,0,0,3,0,0,0,0"/>
   <wangtile tileid="584" wangid="0,0,0,3,0,3,0,0"/>
   <wangtile tileid="585" wangid="0,0,0,0,0,3,0,0"/>
   <wangtile tileid="627" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="629" wangid="0,3,0,3,0,0,0,0"/>
   <wangtile tileid="630" wangid="0,3,0,3,0,3,0,3"/>
   <wangtile tileid="631" wangid="0,0,0,0,0,3,0,3"/>
   <wangtile tileid="632" wangid="0,1,0,1,0,2,0,2"/>
   <wangtile tileid="675" wangid="0,3,0,0,0,0,0,0"/>
   <wangtile tileid="676" wangid="0,3,0,0,0,0,0,3"/>
   <wangtile tileid="677" wangid="0,0,0,0,0,0,0,3"/>
   <wangtile tileid="720" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="721" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="722" wangid="0,2,0,1,0,1,0,2"/>
   <wangtile tileid="724" wangid="0,0,0,1,0,0,0,2"/>
  </wangset>
 </wangsets>
</tileset>
