<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="master_everything" tilewidth="16" tileheight="16" tilecount="5175" columns="115">
 <image source="master_everything.png" width="1840" height="720"/>
 <tile id="116">
  <animation>
   <frame tileid="116" duration="100"/>
   <frame tileid="119" duration="100"/>
   <frame tileid="122" duration="100"/>
  </animation>
 </tile>
 <tile id="126">
  <animation>
   <frame tileid="126" duration="100"/>
   <frame tileid="129" duration="100"/>
   <frame tileid="132" duration="100"/>
  </animation>
 </tile>
 <tile id="576">
  <animation>
   <frame tileid="576" duration="120"/>
   <frame tileid="579" duration="120"/>
   <frame tileid="582" duration="120"/>
  </animation>
 </tile>
 <tile id="577">
  <animation>
   <frame tileid="577" duration="120"/>
   <frame tileid="580" duration="120"/>
   <frame tileid="583" duration="120"/>
  </animation>
 </tile>
 <tile id="691">
  <animation>
   <frame tileid="691" duration="100"/>
   <frame tileid="694" duration="100"/>
   <frame tileid="697" duration="100"/>
  </animation>
 </tile>
 <tile id="806">
  <animation>
   <frame tileid="806" duration="100"/>
   <frame tileid="809" duration="100"/>
   <frame tileid="812" duration="100"/>
  </animation>
 </tile>
 <tile id="807">
  <animation>
   <frame tileid="807" duration="100"/>
   <frame tileid="810" duration="100"/>
   <frame tileid="813" duration="100"/>
  </animation>
 </tile>
 <tile id="1037">
  <animation>
   <frame tileid="1037" duration="80"/>
   <frame tileid="1040" duration="80"/>
   <frame tileid="1043" duration="80"/>
  </animation>
 </tile>
 <tile id="1152">
  <animation>
   <frame tileid="1152" duration="80"/>
   <frame tileid="1155" duration="80"/>
   <frame tileid="1158" duration="80"/>
  </animation>
 </tile>
 <tile id="1266">
  <animation>
   <frame tileid="1266" duration="80"/>
   <frame tileid="1269" duration="80"/>
   <frame tileid="1272" duration="80"/>
  </animation>
 </tile>
 <tile id="1267">
  <animation>
   <frame tileid="1267" duration="80"/>
   <frame tileid="1270" duration="80"/>
   <frame tileid="1273" duration="80"/>
  </animation>
 </tile>
 <tile id="1268">
  <animation>
   <frame tileid="1268" duration="80"/>
   <frame tileid="1271" duration="80"/>
   <frame tileid="1274" duration="80"/>
  </animation>
 </tile>
 <tile id="1381">
  <animation>
   <frame tileid="1381" duration="120"/>
   <frame tileid="1384" duration="120"/>
   <frame tileid="1387" duration="120"/>
  </animation>
 </tile>
 <tile id="1383">
  <animation>
   <frame tileid="1383" duration="80"/>
   <frame tileid="1386" duration="80"/>
   <frame tileid="1389" duration="80"/>
  </animation>
 </tile>
 <tile id="1496">
  <animation>
   <frame tileid="1496" duration="80"/>
   <frame tileid="1499" duration="80"/>
   <frame tileid="1502" duration="80"/>
  </animation>
 </tile>
 <tile id="1497">
  <animation>
   <frame tileid="1497" duration="80"/>
   <frame tileid="1500" duration="80"/>
   <frame tileid="1503" duration="80"/>
  </animation>
 </tile>
 <tile id="1498">
  <animation>
   <frame tileid="1498" duration="80"/>
   <frame tileid="1501" duration="80"/>
   <frame tileid="1504" duration="80"/>
  </animation>
 </tile>
 <tile id="1736">
  <animation>
   <frame tileid="1736" duration="100"/>
   <frame tileid="1739" duration="100"/>
   <frame tileid="1742" duration="100"/>
  </animation>
 </tile>
 <tile id="1737">
  <animation>
   <frame tileid="1737" duration="100"/>
   <frame tileid="1740" duration="100"/>
   <frame tileid="1743" duration="100"/>
  </animation>
 </tile>
 <tile id="1738">
  <animation>
   <frame tileid="1738" duration="80"/>
   <frame tileid="1741" duration="80"/>
   <frame tileid="1744" duration="80"/>
  </animation>
 </tile>
 <tile id="2186">
  <animation>
   <frame tileid="2192" duration="100"/>
   <frame tileid="2189" duration="100"/>
   <frame tileid="2192" duration="100"/>
  </animation>
 </tile>
 <tile id="2187">
  <animation>
   <frame tileid="2187" duration="80"/>
   <frame tileid="2190" duration="80"/>
   <frame tileid="2193" duration="80"/>
  </animation>
 </tile>
 <tile id="2188">
  <animation>
   <frame tileid="2188" duration="100"/>
   <frame tileid="2191" duration="100"/>
   <frame tileid="2194" duration="100"/>
  </animation>
 </tile>
 <tile id="2196">
  <animation>
   <frame tileid="2196" duration="120"/>
   <frame tileid="2199" duration="120"/>
   <frame tileid="2202" duration="120"/>
  </animation>
 </tile>
 <tile id="2301">
  <animation>
   <frame tileid="2301" duration="100"/>
   <frame tileid="2304" duration="100"/>
   <frame tileid="2307" duration="100"/>
  </animation>
 </tile>
 <tile id="2302">
  <animation>
   <frame tileid="2302" duration="80"/>
   <frame tileid="2305" duration="80"/>
   <frame tileid="2308" duration="80"/>
  </animation>
 </tile>
 <tile id="2303">
  <animation>
   <frame tileid="2303" duration="100"/>
   <frame tileid="2306" duration="100"/>
   <frame tileid="2309" duration="100"/>
  </animation>
 </tile>
 <tile id="2311">
  <animation>
   <frame tileid="2311" duration="120"/>
   <frame tileid="2314" duration="120"/>
   <frame tileid="2317" duration="120"/>
  </animation>
 </tile>
 <tile id="2416">
  <animation>
   <frame tileid="2416" duration="100"/>
   <frame tileid="2419" duration="100"/>
   <frame tileid="2422" duration="100"/>
  </animation>
 </tile>
 <tile id="2417">
  <animation>
   <frame tileid="2417" duration="100"/>
   <frame tileid="2420" duration="100"/>
   <frame tileid="2423" duration="100"/>
  </animation>
 </tile>
 <tile id="2418">
  <animation>
   <frame tileid="2418" duration="100"/>
   <frame tileid="2421" duration="100"/>
   <frame tileid="2424" duration="100"/>
  </animation>
 </tile>
 <tile id="2426">
  <animation>
   <frame tileid="2426" duration="120"/>
   <frame tileid="2429" duration="120"/>
   <frame tileid="2432" duration="120"/>
  </animation>
 </tile>
 <wangsets>
  <wangset name="Unnamed Set" type="corner" tile="-1">
   <wangcolor name="" color="#ff0000" tile="-1" probability="1"/>
  </wangset>
 </wangsets>
</tileset>
