<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="dungeon_tiles" tilewidth="16" tileheight="16" tilecount="552" columns="23">
 <image source="dungeon_tiles.png" trans="ffffff" width="368" height="384"/>
 <wangsets>
  <wangset name="islands" type="corner" tile="-1">
   <wangcolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="" color="#00ff00" tile="-1" probability="1"/>
   <wangcolor name="" color="#0000ff" tile="-1" probability="1"/>
   <wangcolor name="" color="#ff7700" tile="-1" probability="1"/>
   <wangtile tileid="53" wangid="0,3,0,4,0,3,0,3"/>
   <wangtile tileid="54" wangid="0,3,0,4,0,4,0,3"/>
   <wangtile tileid="55" wangid="0,3,0,3,0,4,0,3"/>
   <wangtile tileid="64" wangid="0,2,0,1,0,2,0,2"/>
   <wangtile tileid="65" wangid="0,2,0,1,0,1,0,2"/>
   <wangtile tileid="66" wangid="0,2,0,2,0,1,0,2"/>
   <wangtile tileid="78" wangid="0,3,0,3,0,4,0,4"/>
   <wangtile tileid="87" wangid="0,1,0,1,0,2,0,2"/>
   <wangtile tileid="88" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="89" wangid="0,2,0,2,0,1,0,1"/>
   <wangtile tileid="100" wangid="0,4,0,3,0,3,0,4"/>
   <wangtile tileid="101" wangid="0,3,0,3,0,3,0,4"/>
   <wangtile tileid="110" wangid="0,1,0,2,0,2,0,2"/>
   <wangtile tileid="111" wangid="0,1,0,2,0,2,0,1"/>
   <wangtile tileid="112" wangid="0,2,0,2,0,2,0,1"/>
  </wangset>
 </wangsets>
</tileset>
