<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="tiles" tilewidth="16" tileheight="16" tilecount="350" columns="25">
 <image source="../assets/tiles.png" width="400" height="224"/>
 <wangsets>
  <wangset name="ground" type="corner" tile="-1">
   <wangcolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="island" color="#00ff00" tile="-1" probability="1"/>
  </wangset>
 </wangsets>
</tileset>
